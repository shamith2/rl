#!/bin/bash

sudo apt update && sudo apt install cmake swig libopenmpi-dev python3-dev zlib1g-dev

virtualenv baselines --python=python3

. baselines/bin/activate

cd baselines

python -m pip install tensorflow

pip install -e .

pip install Box2D box2d-py

cd ..

deactivate
